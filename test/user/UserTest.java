package user;

import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;
import org.junit.Test;

public class UserTest {


    @Test
    public void givenUserWhenInvokingRandomNameThenTheNameShouldBeChanged() {
        User user = new User();
        String currentName = user.name;

        user = new User();

        assertNotEquals(currentName, user.name);
    }

    @Test
    public void givenUserWhenInvokingRandomNameThenTheLengthOfTheNameShouldBeBetween6And10() {
        User user = new User();

        boolean result = user.name.length() >= 6 && user.name.length() <= 10;

        assertTrue(result);
    }


}
