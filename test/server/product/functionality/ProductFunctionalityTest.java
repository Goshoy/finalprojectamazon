package server.product.functionality;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import org.junit.Test;
import server.log.system.LogSystem;
import user.Client;

public class ProductFunctionalityTest {

    @Test
    public void givenProductFunctionalityWhenAddingNewProductAndNewPublisherThenReturnTrue() {

        LogSystem logSystem = mock(LogSystem.class);
        ProductFunctionality functionality = new ProductFunctionality();
        when(logSystem.getCurrentUser()).thenReturn(new Client("gosho", "1234"));


        functionality.addProduct("shoes 1 50 very good condition", logSystem);
        int id = functionality.getCurrentId();

        assertTrue(functionality.containsProduct(id));
    }

    @Test
    public void givenProductFunctionalityWhenAddingNewProductToExistingPublisherThenReturnTrue() {
        LogSystem logSystem = mock(LogSystem.class);
        ProductFunctionality functionality = new ProductFunctionality();
        when(logSystem.getCurrentUser()).thenReturn(new Client("gosho", "1234"));

        functionality.addProduct("shoes 1 50 very good condition", logSystem);
        functionality.addProduct("sandals 1 50 very good condition", logSystem);
        int id = functionality.getCurrentId();

        assertTrue(functionality.containsProduct(id));
    }

    @Test
    public void givenProductFunctionalityWhenAddingProductToExistingPublisherThenReturnTrue() {
        LogSystem logSystem = mock(LogSystem.class);
        ProductFunctionality functionality = new ProductFunctionality();
        when(logSystem.getCurrentUser()).thenReturn(new Client("gosho", "1234"));

        functionality.addProduct("shoes 1 50 very good condition", logSystem);
        functionality.addProduct("shoes 1 50 very good condition", logSystem);
        functionality.addProduct("sandals 1 50 very good condition", logSystem);
        functionality.addProduct("sandals 1 50 very good condition", logSystem);
        int id = functionality.getCurrentId();

        assertTrue(functionality.containsProduct(id));
    }

    @Test
    public void givenProductFunctionalityWhenAddingProductTwiceToExistingPublisherThenReturnTrue() {
        LogSystem logSystem = mock(LogSystem.class);
        ProductFunctionality functionality = new ProductFunctionality();
        when(logSystem.getCurrentUser()).thenReturn(new Client("gosho", "1234"));

        functionality.addProduct("shoes 1 50 very good condition", logSystem);
        functionality.addProduct("shoes 1 50 very good condition", logSystem);
        int id = functionality.getCurrentId();
        int quantity = functionality.getProductsByPublisher().get(logSystem.getCurrentUser())
                .getQuantityOfProductForSale(id);

        assertEquals(2, quantity);
    }

    @Test
    public void givenProductFunctionalityWhenAddingProductFiveTimesToExistingPublisherThenReturnTrue() {
        LogSystem logSystem = mock(LogSystem.class);
        ProductFunctionality functionality = new ProductFunctionality();
        when(logSystem.getCurrentUser()).thenReturn(new Client("gosho", "1234"));

        functionality.addProduct("shoes 1 50 very good condition", logSystem);
        functionality.addProduct("shoes 1 50 very good condition", logSystem);
        functionality.addProduct("shoes 1 50 very good condition", logSystem);
        functionality.addProduct("shoes 1 50 very good condition", logSystem);
        functionality.addProduct("shoes 1 50 very good condition", logSystem);
        int id = functionality.getCurrentId();
        int quantity = functionality.getProductsByPublisher().get(logSystem.getCurrentUser())
                .getQuantityOfProductForSale(id);

        assertEquals(5, quantity);
    }

    @Test
    public void givenProductFunctionalityWhenAddingNewProductThreeTimesToExistingPublisherThenReturnTrue() {
        LogSystem logSystem = mock(LogSystem.class);
        ProductFunctionality functionality = new ProductFunctionality();
        when(logSystem.getCurrentUser()).thenReturn(new Client("gosho", "1234"));

        functionality.addProduct("shoes 1 50 very good condition", logSystem);
        functionality.addProduct("shoes 1 50 very good condition", logSystem);
        functionality.addProduct("sandals 1 50 very good condition", logSystem);
        functionality.addProduct("sandals 1 50 very good condition", logSystem);
        functionality.addProduct("sandals 1 50 very good condition", logSystem);
        int id = functionality.getCurrentId();
        int quantity = functionality.getProductsByPublisher().get(logSystem.getCurrentUser())
                .getQuantityOfProductForSale(id);

        assertEquals(3, quantity);
    }

    @Test
    public void givenProductFunctionalityWhenAddingNewProductThreeTimesAtOnceToExistingPublisherThenReturnTrue() {
        LogSystem logSystem = mock(LogSystem.class);
        ProductFunctionality functionality = new ProductFunctionality();
        when(logSystem.getCurrentUser()).thenReturn(new Client("gosho", "1234"));

        functionality.addProduct("shoes 1 50 very good condition", logSystem);
        functionality.addProduct("shoes 1 50 very good condition", logSystem);
        functionality.addProduct("sandals 3 50 very good condition", logSystem);
        int id = functionality.getCurrentId();
        int quantity = functionality.getProductsByPublisher().get(logSystem.getCurrentUser())
                .getQuantityOfProductForSale(id);

        assertEquals(3, quantity);
    }

    @Test
    public void givenProductFunctionalityWhenAddingOneNewProductThreeTimesAtOnceToNotExistingPublisherThenReturnTrue() {
        LogSystem logSystem = mock(LogSystem.class);
        ProductFunctionality functionality = new ProductFunctionality();
        when(logSystem.getCurrentUser()).thenReturn(new Client("gosho", "1234"));

        functionality.addProduct("sandals 3 50 very good condition", logSystem);
        int id = functionality.getCurrentId();
        int quantity = functionality.getProductsByPublisher().get(logSystem.getCurrentUser())
                .getQuantityOfProductForSale(id);

        assertEquals(3, quantity);
    }

    @Test
    public void givenProductFunctionalityWhenAddingProductThreeTimesAtOnceToExistingPublisherThenReturnTrue() {
        LogSystem logSystem = mock(LogSystem.class);
        ProductFunctionality functionality = new ProductFunctionality();
        when(logSystem.getCurrentUser()).thenReturn(new Client("gosho", "1234"));

        functionality.addProduct("shoes 1 50 very good condition", logSystem);
        functionality.addProduct("shoes 3 50 very good condition", logSystem);
        int id = functionality.getCurrentId();
        int quantity = functionality.getProductsByPublisher().get(logSystem.getCurrentUser())
                .getQuantityOfProductForSale(id);

        assertEquals(4, quantity);
    }

    @Test
    public void givenProductFunctionalityWhenBuyingProductWith3QuantityThenTheProductQuantityShouldBe2() {
        LogSystem logSystem = mock(LogSystem.class);
        ProductFunctionality functionality = new ProductFunctionality();
        when(logSystem.getCurrentUser()).thenReturn(new Client("gosho", "1234"));

        functionality.addProduct("shoes 3 50 very good condition", logSystem);
        int id = functionality.getCurrentId();

        when(logSystem.getCurrentUser()).thenReturn(new Client("ivan", "1234"));
        functionality.buy(id, logSystem);

        when(logSystem.getCurrentUser()).thenReturn(new Client("gosho", "1234"));
        int quantity = functionality.getProductsByPublisher().get(logSystem.getCurrentUser())
                .getQuantityOfProductForSale(id);

        assertEquals(2, quantity);
    }

    @Test
    public void givenProductFunctionalityWhenBuyingProductThreeTimesWith3QuantityThenTheProductQuantityShouldBe0() {
        LogSystem logSystem = mock(LogSystem.class);
        ProductFunctionality functionality = new ProductFunctionality();
        when(logSystem.getCurrentUser()).thenReturn(new Client("gosho", "1234"));

        functionality.addProduct("shoes 3 50 very good condition", logSystem);
        int id = functionality.getCurrentId();

        when(logSystem.getCurrentUser()).thenReturn(new Client("ivan", "1234"));
        functionality.buy(id, logSystem);
        functionality.buy(id, logSystem);
        functionality.buy(id, logSystem);

        when(logSystem.getCurrentUser()).thenReturn(new Client("gosho", "1234"));
        int quantity = functionality.getProductsByPublisher().get(logSystem.getCurrentUser())
                .getQuantityOfProductForSale(id);

        assertEquals(0, quantity);
    }

    @Test
    public void givenPFWhenSellingOutProductThenTheProductShouldBeDeleted() {
        LogSystem logSystem = mock(LogSystem.class);
        ProductFunctionality functionality = new ProductFunctionality();
        when(logSystem.getCurrentUser()).thenReturn(new Client("gosho", "1234"));

        functionality.addProduct("shoes 3 50 very good condition", logSystem);
        int id = functionality.getCurrentId();

        when(logSystem.getCurrentUser()).thenReturn(new Client("ivan", "1234"));
        functionality.buy(id, logSystem);
        functionality.buy(id, logSystem);
        functionality.buy(id, logSystem);

        when(logSystem.getCurrentUser()).thenReturn(new Client("gosho", "1234"));
        boolean result = functionality.containsProduct(id);

        assertFalse(result);
    }

    @Test
    public void givenPFWhenSellingTillProductQuantityIsOneThenReturnTrue() {
        LogSystem logSystem = mock(LogSystem.class);
        ProductFunctionality functionality = new ProductFunctionality();
        when(logSystem.getCurrentUser()).thenReturn(new Client("gosho", "1234"));

        functionality.addProduct("shoes 3 50 very good condition", logSystem);
        int id = functionality.getCurrentId();

        when(logSystem.getCurrentUser()).thenReturn(new Client("ivan", "1234"));
        functionality.buy(id, logSystem);
        functionality.buy(id, logSystem);

        when(logSystem.getCurrentUser()).thenReturn(new Client("gosho", "1234"));
        boolean result = functionality.containsProduct(id);

        assertTrue(result);
    }

    @Test
    public void givenPFWhenDeletingProductThenTheProductShouldBeDeleted() {
        LogSystem logSystem = mock(LogSystem.class);
        ProductFunctionality functionality = new ProductFunctionality();
        when(logSystem.getCurrentUser()).thenReturn(new Client("gosho", "1234"));

        functionality.addProduct("shoes 3 50 very good condition", logSystem);
        int id = functionality.getCurrentId();
        functionality.deleteProduct(id, logSystem);

        boolean result = functionality.containsProduct(id);

        assertFalse(result);
    }

    @Test
    public void givenPFWhenDeletingProductWithoutPermissionThenTheProductShouldNotBeDeleted() {
        LogSystem logSystem = mock(LogSystem.class);
        ProductFunctionality functionality = new ProductFunctionality();
        when(logSystem.getCurrentUser()).thenReturn(new Client("gosho", "1234"));

        functionality.addProduct("shoes 3 50 very good condition", logSystem);
        int id = functionality.getCurrentId();

        when(logSystem.getCurrentUser()).thenReturn(new Client("ivan", "1234"));
        functionality.deleteProduct(id, logSystem);

        boolean result = functionality.containsProduct(id);

        assertTrue(result);
    }



}
