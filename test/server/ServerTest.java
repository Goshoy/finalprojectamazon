package server;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import org.junit.Test;
import user.Client;
import user.Moderator;

public class ServerTest {

    private final String pathToTestFile =
            "C:\\Users\\Goshoy\\eclipse-workspace\\FinalProjectAmazon\\usersTest.txt";

    @Test
    public void givenServerWhenSigningUpNewUserThenTheUserShouldBeAdded() {
        Server server = new Server();

        server.SignUp("Ivan", "12345");
        boolean result = server.containsUser("Ivan");


        assertTrue(result);
    }

    @Test
    public void givenServerWhenSigningUpExistingUserThenTheUserShouldNotBeAddedAgain() {
        Server server = new Server();
        server.SignUp("Ivan", "12345");
        boolean result = server.SignUp("Ivan", "12346");

        assertFalse(result);
    }

    @Test
    public void givenFileWithUsersWhenCreatingServerThenTheUsersShouldBeAdded() {
        Server server = new Server();
        server.SignUp("Ivan", "1234");
        server.SignUp("Niki", "1234");

        boolean result = server.containsUser("Ivan") && server.containsUser("Niki");

        assertTrue(result);
    }

    @Test
    public void givenServerWhenSigningInIntoExistingAccountThenTheCurrentUserShouldBeChanged() {
        Server server = new Server();
        server.SignUp("Ivan", "1234");

        server.SignIn("Ivan", "1234");
        boolean result = server.getCurrentUser().equals(new Client("Ivan", "1234"));

        assertTrue(result);
    }

    @Test
    public void givenServerWhenSigningInIntoNotExistingAccountThenReturnFalse() {
        Server server = new Server();
        server.SignUp("Ivan", "1234");

        boolean result = server.SignIn("Iva", "1234");

        assertFalse(result);
    }

    @Test
    public void givenServerWhenSigningWithWrongPasswordThenReturnFalse() {
        Server server = new Server();
        server.SignUp("Ivan", "1234");

        boolean result = server.SignIn("Ivan", "1235");

        assertFalse(result);
    }

    @Test
    public void givenServerWhenDeletingExistingUserThenTheUserShouldBeDeleted() {
        Server server = new Server();
        server.SignUp("Ivan", "1234");

        server.SignIn("Ivan", "1234");
        server.deleteCurrentUser();
        boolean result = server.containsUser("Ivan");

        assertFalse(result);
    }

    @Test
    public void givenServerWhenDeletingExistingUserThenCurrentUserShouldBeChanged() {
        Server server = new Server();
        server.SignUp("Ivan", "1234");

        server.SignIn("Ivan", "1234");
        server.deleteCurrentUser();
        boolean result = server.getCurrentUser().getName().equals("Ivan");

        assertFalse(result);
    }

    @Test
    public void givenServerWhenDeletingNotExistingUserThenReturnFalse() {
        Server server = new Server();
        server.SignUp("Ivan", "1234");

        server.deleteCurrentUser();
        boolean result = server.getCurrentUser().getName().equals("Ivan");

        assertFalse(result);
    }

    @Test
    public void givenServerWhenDeletingNotExistingUserThenTheCurrentUserShouldNotBeChanged() {
        Server server = new Server();
        server.SignUp("Ivan", "1234");

        String currentUserName = server.getCurrentUser().getName();
        server.deleteCurrentUser();
        boolean result = server.getCurrentUser().getName().equals(currentUserName);

        assertFalse(result);
    }

    @Test
    public void givenServerWhenWritingMessageToClientThenTheMessageShouldBeAddedAsNew() {
        Server server = new Server();
        server.SignUp("Ivan", "1234");
        server.SignUp("Niki", "1234");

        server.SignIn("Ivan", "1234");
        server.writeMessage("Niki", "hi");
        server.SignIn("Niki", "1234");
        boolean result = server.getCurrentUser().getNewMessages().length == 1;

        assertTrue(result);
    }

    @Test
    public void givenServerWhenWriting5MessagesToClientAndReadingAllThenTheClientShouldHave0Messages() {
        Server server = new Server();
        server.SignUp("Ivan", "1234");
        server.SignUp("Niki", "1234");

        server.SignIn("Ivan", "1234");
        for (int i = 0; i < 5; i++) {
            server.writeMessage("Niki", "hi");
        }
        server.SignIn("Niki", "1235");
        server.getCurrentUser().getNewMessages();
        boolean result = server.getCurrentUser().getNewMessages().length == 0;

        assertTrue(result);
    }

    @Test
    public void givenServerWhenWriting10MessagesToClientThenTheClientShouldTurnIntoModerator() {
        Server server = new Server();
        server.SignUp("Ivan", "1234");
        server.SignUp("Niki", "1234");

        server.SignIn("Ivan", "1234");
        for (int i = 0; i < 15; i++) {
            server.writeMessage("Niki", "hi");
        }
        server.SignIn("Niki", "1234");
        boolean result = server.getCurrentUser() instanceof Moderator;

        assertTrue(result);
    }

    @Test
    public void givenServerWhenWriting15MessagesToClientThenTheClientShouldTurnIntoModeratorAndHave15Messages() {
        Server server = new Server();
        server.SignUp("Ivan", "1234");
        server.SignUp("Niki", "1234");

        server.SignIn("Ivan", "1234");
        for (int i = 0; i < 15; i++) {
            server.writeMessage("Niki", "hi");
        }
        server.SignIn("Niki", "1234");
        boolean result = server.getCurrentUser().getNewMessages().length == 15;

        assertTrue(result);
    }

    @Test
    public void givenServerWhenWriting15MessagesToClientAndReadingAllThenTheClientShouldTurnIntoModeratorAndHave0Messages() {
        Server server = new Server();
        server.SignUp("Ivan", "1234");
        server.SignUp("Niki", "1234");

        server.SignIn("Ivan", "1234");
        for (int i = 0; i < 15; i++) {
            server.writeMessage("Niki", "hi");
        }
        server.SignIn("Niki", "1235");
        server.getCurrentUser().getNewMessages();
        boolean result = server.getCurrentUser().getNewMessages().length == 0;

        assertTrue(result);
    }

}
