package server.command.parser;

import static org.junit.Assert.assertTrue;
import org.junit.Test;
import server.CommandAddProduct;
import server.CommandBuy;
import server.CommandDeleteMyProfile;
import server.CommandDeleteProduct;
import server.CommandFunctionality;
import server.CommandLogOut;
import server.CommandSeeAllNewMsgs;
import server.CommandShow;
import server.CommandShowAllProducts;
import server.CommandShowProduct;
import server.CommandShowProductIcon;
import server.CommandShowStatistics;
import server.CommandSignIn;
import server.CommandSignUp;
import server.CommandWriteMsg;

public class CommandParserMainTest {

    CommandParserMain parser = new CommandParserMain();

    @Test
    public void givenCommandParserWhenAskingForCommandSingInInstanceThenReturnTrue() {
        CommandFunctionality command = parser.generateInstance("signIn");
        boolean result = command instanceof CommandSignIn;

        assertTrue(result);
    }

    @Test
    public void givenCommandParserWhenAskingForCommandSingUpInstanceThenReturnTrue() {
        CommandFunctionality command = parser.generateInstance("signUp");
        boolean result = command instanceof CommandSignUp;

        assertTrue(result);
    }

    @Test
    public void givenCommandParserWhenAskingForCommandShowInstanceThenReturnTrue() {
        CommandFunctionality command = parser.generateInstance("show");
        boolean result = command instanceof CommandShow;

        assertTrue(result);
    }

    @Test
    public void givenCommandParserWhenAskingForCommandSeeAllNewMessagesInstanceThenReturnTrue() {
        CommandFunctionality command = parser.generateInstance("seeAllNewMessages");
        boolean result = command instanceof CommandSeeAllNewMsgs;

        assertTrue(result);
    }

    @Test
    public void givenCommandParserWhenAskingForCommandWriteMsgInstanceThenReturnTrue() {
        CommandFunctionality command = parser.generateInstance("writeMessage");
        boolean result = command instanceof CommandWriteMsg;

        assertTrue(result);
    }

    @Test
    public void givenCommandParserWhenAskingForCommandDeleteMyProfileInstanceThenReturnTrue() {
        CommandFunctionality command = parser.generateInstance("deleteMyProfile");
        boolean result = command instanceof CommandDeleteMyProfile;

        assertTrue(result);
    }

    @Test
    public void givenCommandParserWhenAskingForCommandLogOutInstanceThenReturnTrue() {
        CommandFunctionality command = parser.generateInstance("logout");
        boolean result = command instanceof CommandLogOut;

        assertTrue(result);
    }

    @Test
    public void givenCommandParserWhenAskingForNotExistingCommandInstanceThenReturnTrue() {
        CommandFunctionality command = parser.generateInstance("login");
        boolean result = command == null;

        assertTrue(result);
    }

    @Test
    public void givenCommandParserWhenAskingForCommandBuyInstanceThenReturnTrue() {
        CommandFunctionality command = parser.generateInstance("buy");
        boolean result = command instanceof CommandBuy;

        assertTrue(result);
    }

    @Test
    public void givenCommandParserWhenAskingForCommandDeleteProductInstanceThenReturnTrue() {
        CommandFunctionality command = parser.generateInstance("deleteProduct");
        boolean result = command instanceof CommandDeleteProduct;

        assertTrue(result);
    }

    @Test
    public void givenCommandParserWhenAskingForCommandShowAllProductsInstanceThenReturnTrue() {
        CommandFunctionality command = parser.generateInstance("showAllProducts");
        boolean result = command instanceof CommandShowAllProducts;

        assertTrue(result);
    }

    @Test
    public void givenCommandParserWhenAskingForCommandShowProductInstanceThenReturnTrue() {
        CommandFunctionality command = parser.generateInstance("showProduct");
        boolean result = command instanceof CommandShowProduct;

        assertTrue(result);
    }

    @Test
    public void givenCommandParserWhenAskingForCommandShowProductIconInstanceThenReturnTrue() {
        CommandFunctionality command = parser.generateInstance("showProductIcon");
        boolean result = command instanceof CommandShowProductIcon;

        assertTrue(result);
    }

    @Test
    public void givenCommandParserWhenAskingForCommandAddProductInstanceThenReturnTrue() {
        CommandFunctionality command = parser.generateInstance("addProduct");
        boolean result = command instanceof CommandAddProduct;

        assertTrue(result);
    }

    @Test
    public void givenCommandParserWhenAskingForCommandShowStatisticsInstanceThenReturnTrue() {
        CommandFunctionality command = parser.generateInstance("showStatistics");
        boolean result = command instanceof CommandShowStatistics;

        assertTrue(result);
    }
}
