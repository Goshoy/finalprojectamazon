package server.log.system;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import org.junit.Test;
import user.User;


public class LogSystemTest {

    LogSystem logSystem;

    @Test
    public void givenLogSystemWithUsersWhenDeletingUserThenTheUsersShouldBeDeleted() {
        this.logSystem = new LogSystem();
        this.logSystem.signUp("user", "1234");

        this.logSystem.signIn("user", "1234");
        this.logSystem.deleteCurrentUser();
        boolean result = logSystem.containsUser("user");

        assertFalse(result);
    }

    @Test
    public void givenLogSystemWithUsersWhenDeletingUserThenTheCurrentUserShouldBeChanged() {
        this.logSystem = new LogSystem();
        this.logSystem.signUp("user", "1234");

        this.logSystem.signIn("user", "1234");
        this.logSystem.deleteCurrentUser();
        boolean result = logSystem.getCurrentUser().getName().equals("user");

        assertFalse(result);
    }

    @Test
    public void givenLogSystemWhenSigningInThenTheCurrentUserShouldBeChanged() {
        this.logSystem = new LogSystem();
        this.logSystem.signUp("user", "1234");

        this.logSystem.signIn("user", "1234");
        boolean result = logSystem.getCurrentUser().getName().equals("user");

        assertTrue(result);
    }

    @Test
    public void givenLogSystemWhenSigningInWithWrongPasswordThenTheCurrentUserShouldNotBeChanged() {
        this.logSystem = new LogSystem();
        User current = logSystem.getCurrentUser();
        this.logSystem.signUp("user", "1234");

        this.logSystem.signIn("user", "1233");
        boolean result = logSystem.getCurrentUser().equals(current);

        assertTrue(result);
    }

    @Test
    public void givenLogSystemWhenSigningInWithWrongNameThenTheCurrentUserShouldNotBeChanged() {
        this.logSystem = new LogSystem();
        User current = logSystem.getCurrentUser();
        this.logSystem.signUp("user", "1234");

        this.logSystem.signIn("user2", "1234");
        boolean result = logSystem.getCurrentUser().equals(current);

        assertTrue(result);
    }

}
