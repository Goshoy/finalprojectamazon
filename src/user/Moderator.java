package user;

import java.util.Arrays;

public class Moderator extends Client {

    private final short MAX_NUMBER = 100;
    private int msgCount;
    private int newMessages;
    private String[] messages;

    public Moderator(String name, String password) {
        super(name, password);
        this.messages = new String[this.MAX_NUMBER];
        this.msgCount = 0;
        this.newMessages = 0;
    }

    public boolean enoughSpaceForMessage() {
        if (this.msgCount + 1 > this.MAX_NUMBER) {
            if (this.newMessages > 0) {
                for (int i = 0; i < this.newMessages; i++) {
                    this.messages[i] = this.messages[this.msgCount - i];
                }
            }
            this.msgCount = this.newMessages - 1;
        }
        return true;
    }

    public void addNewMessage(String message) {
        this.messages[this.msgCount++] = message;
        this.newMessages++;
    }

    public int getNumberOfNewMsgs() {
        return this.newMessages;
    }

    public String[] getNewMessages() {
        int numberOfMessages = this.newMessages;
        this.newMessages = 0;
        return Arrays.copyOfRange(this.messages, this.msgCount - numberOfMessages, this.msgCount);
    }

    public void show() {
        System.out.println("Name : " + this.name + ".");
        System.out.println("New messages " + this.newMessages + ".");
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + MAX_NUMBER;
        result = prime * result + Arrays.hashCode(messages);
        result = prime * result + msgCount;
        result = prime * result + newMessages;
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        Moderator other = (Moderator) obj;
        if (MAX_NUMBER != other.MAX_NUMBER)
            return false;
        if (!Arrays.equals(messages, other.messages))
            return false;
        if (msgCount != other.msgCount)
            return false;
        if (newMessages != other.newMessages)
            return false;
        return true;
    }

}
