package user;

import java.util.Arrays;

public class Client extends User {

    private final short MAX_NUMBER = 10;
    private final String password;
    // The max number messages a client can have is 10
    protected String[] messages;
    private short msgCount;
    private int newMessages;

    public Client(String name, String password) {
        super.name = name;
        this.password = password;
        this.messages = new String[MAX_NUMBER];
        this.msgCount = 0;
        this.newMessages = 0;
    }

    public String getPassword() {
        return new String(this.password);
    }

    public boolean correctPassword(String password) {
        return this.password.equals(password);
    }

    public boolean enoughSpaceForMessage() {
        return (this.msgCount + 1) < this.MAX_NUMBER;
    }

    public void addNewMessage(String message) {
        this.messages[this.msgCount++] = message;
        this.newMessages++;
    }

    public int getNumberOfNewMsgs() {
        return this.newMessages;
    }

    public String[] getNewMessages() {
        int numberOfMessages = this.newMessages;
        this.newMessages = 0;
        return Arrays.copyOfRange(this.messages, this.msgCount - numberOfMessages, this.msgCount);
    }

    public void show() {
        System.out.println("Name : " + this.name + ".");
        System.out.println("New messages " + this.newMessages + ".");
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((password == null) ? 0 : password.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        Client other = (Client) obj;
        if (password == null) {
            if (other.password != null)
                return false;
        } else if (!password.equals(other.password))
            return false;
        return true;
    }

}
