package user;

import java.io.Serializable;
import java.util.Random;

public class User implements Serializable {
    String name;

    public User() {
        this.name = "guest" + randomName();
    }

    public String getName() {
        return this.name;
    }

    public boolean enoughSpaceForMessage() {
        return false;
    }

    public void addNewMessage(String message) {}

    public int getNumberOfNewMsgs() {
        return 0;
    }

    public String[] getNewMessages() {
        return null;
    }

    public String getPassword() {
        return null;
    }

    public boolean correctPassword(String password) {
        return false;
    }

    public void show() {
        System.out.println("The user with name " + this.name + " is a guest.");
    }

    private final String randomName() {
        Random random = new Random();
        String generatedString = String.valueOf(random.nextInt(10000));

        return generatedString;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        User other = (User) obj;
        if (name == null) {
            if (other.name != null)
                return false;
        } else if (!name.equals(other.name))
            return false;
        return true;
    }


}
