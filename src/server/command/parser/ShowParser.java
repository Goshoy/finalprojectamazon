package server.command.parser;

import server.CommandFunctionality;
import server.CommandShow;

class ShowParser extends CommandParser {

    @Override
    CommandFunctionality generateInstance() {
        return new CommandShow();
    }

}
