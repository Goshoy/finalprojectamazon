package server.command.parser;

import server.CommandFunctionality;
import server.CommandShowStatistics;

class ShowStatisticsParser extends CommandParser {

    @Override
    CommandFunctionality generateInstance() {
        return new CommandShowStatistics();
    }

}
