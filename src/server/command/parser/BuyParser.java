package server.command.parser;

import server.CommandBuy;
import server.CommandFunctionality;

class BuyParser extends CommandParser {

    @Override
    CommandFunctionality generateInstance() {
        return new CommandBuy();
    }

}
