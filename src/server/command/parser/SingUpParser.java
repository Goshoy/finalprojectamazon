package server.command.parser;

import server.CommandFunctionality;
import server.CommandSignUp;

class SingUpParser extends CommandParser {

    @Override
    CommandFunctionality generateInstance() {
        return new CommandSignUp();
    }

}
