package server.command.parser;

import server.CommandFunctionality;
import server.CommandShowProductIcon;

class ShowProductIconParser extends CommandParser {

    @Override
    CommandFunctionality generateInstance() {
        return new CommandShowProductIcon();
    }

}
