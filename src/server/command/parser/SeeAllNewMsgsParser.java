package server.command.parser;

import server.CommandFunctionality;
import server.CommandSeeAllNewMsgs;

class SeeAllNewMsgsParser extends CommandParser {

    @Override
    CommandFunctionality generateInstance() {
        return new CommandSeeAllNewMsgs();
    }

}
