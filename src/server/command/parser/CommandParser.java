package server.command.parser;

import server.CommandFunctionality;

abstract class CommandParser {

    abstract CommandFunctionality generateInstance();

}
