package server.command.parser;

import server.CommandFunctionality;
import server.CommandSignIn;

class SignInParser extends CommandParser {

    @Override
    CommandFunctionality generateInstance() {
        return new CommandSignIn();
    }

}
