package server.command.parser;

import server.CommandFunctionality;
import server.CommandShowAllProducts;

class ShowAllProductsParser extends CommandParser {

    @Override
    CommandFunctionality generateInstance() {
        return new CommandShowAllProducts();
    }

}
