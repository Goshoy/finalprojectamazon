package server.command.parser;

import server.CommandAddProduct;
import server.CommandFunctionality;

class AddProductParser extends CommandParser {

    @Override
    CommandFunctionality generateInstance() {
        return new CommandAddProduct();
    }

}
