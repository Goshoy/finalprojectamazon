package server.command.parser;

import server.CommandFunctionality;
import server.CommandWriteMsg;

class WriteMsgParser extends CommandParser {

    @Override
    CommandFunctionality generateInstance() {
        return new CommandWriteMsg();
    }

}
