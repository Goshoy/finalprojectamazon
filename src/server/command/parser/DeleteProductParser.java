package server.command.parser;

import server.CommandDeleteProduct;
import server.CommandFunctionality;

class DeleteProductParser extends CommandParser {

    @Override
    CommandFunctionality generateInstance() {
        return new CommandDeleteProduct();
    }

}
