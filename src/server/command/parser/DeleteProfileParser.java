package server.command.parser;

import server.CommandDeleteMyProfile;
import server.CommandFunctionality;

class DeleteProfileParser extends CommandParser {

    @Override
    CommandFunctionality generateInstance() {
        return new CommandDeleteMyProfile();
    }

}
