package server.command.parser;

import server.CommandFunctionality;
import server.CommandLogOut;

class LogOutParser extends CommandParser {

    @Override
    CommandFunctionality generateInstance() {
        return new CommandLogOut();
    }

}
