package server.command.parser;

import server.CommandFunctionality;
import server.CommandShowProduct;

class ShowProductParser extends CommandParser {

    @Override
    CommandFunctionality generateInstance() {
        return new CommandShowProduct();
    }

}
