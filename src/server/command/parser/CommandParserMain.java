package server.command.parser;

import java.util.HashMap;
import java.util.Map;
import server.CommandFunctionality;

public class CommandParserMain {

    protected Map<String, CommandParser> commands = new HashMap<String, CommandParser>();


    public CommandParserMain() {
        commands.put("signIn", new SignInParser());
        commands.put("signUp", new SingUpParser());
        commands.put("logout", new LogOutParser());
        commands.put("writeMessage", new WriteMsgParser());
        commands.put("deleteMyProfile", new DeleteProfileParser());
        commands.put("seeAllNewMessages", new SeeAllNewMsgsParser());
        commands.put("show", new ShowParser());
        commands.put("addProduct", new AddProductParser());
        commands.put("showAllProducts", new ShowAllProductsParser());
        commands.put("buy", new BuyParser());
        commands.put("deleteProduct", new DeleteProductParser());
        commands.put("showProduct", new ShowProductParser());
        commands.put("showProductIcon", new ShowProductIconParser());
        commands.put("showStatistics", new ShowStatisticsParser());
    }

    public CommandFunctionality generateInstance(String command) {
        if (this.commands.containsKey(command)) {
            return commands.get(command).generateInstance();
        }
        return null;
    }
}
