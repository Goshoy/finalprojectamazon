package server.member.functionality;

import java.util.HashMap;
import java.util.Map;
import user.Client;
import user.Moderator;
import user.User;

class MemberAccessParser {

    @SuppressWarnings("rawtypes")
    private Map<Class, Boolean> memberAccessLevel = new HashMap<>();

    MemberAccessParser() {
        memberAccessLevel.put(User.class, false);
        memberAccessLevel.put(Client.class, true);
        memberAccessLevel.put(Moderator.class, true);
    }

    boolean hasAccess(Class type) {
        if (this.memberAccessLevel.containsKey(type)) {
            return this.memberAccessLevel.get(type).booleanValue();
        }
        return false;
    }

}
