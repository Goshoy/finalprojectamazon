package server.member.functionality;

import java.util.Arrays;
import java.util.Map;
import server.log.system.LogSystem;
import server.product.functionality.UserProducts;
import user.Moderator;
import user.User;

public class MemberFunctionality {
    private LogSystem logSystem;
    private final MemberAccessParser memberAccessParser;

    public MemberFunctionality(LogSystem logSystem) {
        this.logSystem = logSystem;
        this.memberAccessParser = new MemberAccessParser();
    }

    public void show(String name) {
        if (this.logSystem.getUsers().containsKey(name)) {
            if (!this.memberAccessParser.hasAccess(this.logSystem.getCurrentUser().getClass())) {
                System.out.println("You have no rights to see user`s information!");
                return;
            }
            this.logSystem.getUsers().get(name).show();
        } else {
            System.out.println("No such user!");
        }
    }

    public void writeMessage(String nameToWriteTo, String message) {
        if (logSystem.getUsers().containsKey(nameToWriteTo)) {
            if (logSystem.getUsers().get(nameToWriteTo).enoughSpaceForMessage()) {
                logSystem.getUsers().get(nameToWriteTo).addNewMessage(message);
            } else {
                makeClientModerator(nameToWriteTo, message);
            }
        } else {
            System.out.println("User not found!");
        }

    }

    private void makeClientModerator(String nameToWriteTo, String message) {
        Moderator moderator =
                new Moderator(nameToWriteTo, logSystem.getUsers().get(nameToWriteTo).getPassword());
        String[] oldMessages = logSystem.getUsers().get(nameToWriteTo).getNewMessages();
        for (int i = 0; i < oldMessages.length; i++) {
            moderator.addNewMessage(oldMessages[i]);
        }
        logSystem.getUsers().remove(nameToWriteTo);
        logSystem.getUsers().put(nameToWriteTo, moderator);
        logSystem.getUsers().get(nameToWriteTo).addNewMessage(message);
        System.out.println(nameToWriteTo + " is now moderator!");
    }

    public void seeAllNewMessages() {
        Arrays.asList(this.logSystem.getCurrentUser().getNewMessages()).stream()
                .forEach(msg -> System.out.println(msg));
    }

    public void showStatistics(Map<User, UserProducts> users) {
        if (users.containsKey(logSystem.getCurrentUser())) {
            System.out.println("Avarage price of products for sale: "
                    + users.get(logSystem.getCurrentUser()).getAveragePriceOfProductForSale());
            System.out.println("Avarage price of bought products: "
                    + users.get(logSystem.getCurrentUser()).getAveragePriceOfBoughtProducts());
        } else {
            System.out.println("No such user!");
        }
    }

}

