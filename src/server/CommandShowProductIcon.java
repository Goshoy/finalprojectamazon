package server;

public class CommandShowProductIcon implements CommandFunctionality {

    @Override
    public void execute(String line, Server server) {
        int id = 0;

        try {
            id = Integer.valueOf(line);
        } catch (NumberFormatException e) {
            System.out.println("Invalid product id!");
            return;
        }

        server.showProductIcon(id);
    }

}
