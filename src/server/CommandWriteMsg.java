package server;

public class CommandWriteMsg implements CommandFunctionality {

    @Override
    public void execute(String line, Server server) {
        int indexOfFirstSpace = line.indexOf(" ");
        String name = line.substring(0, indexOfFirstSpace);
        String message = line.substring(indexOfFirstSpace + 1, line.length());
        server.writeMessage(name, message);
    }

}
