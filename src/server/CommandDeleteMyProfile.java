package server;

public class CommandDeleteMyProfile implements CommandFunctionality {

    @Override
    public void execute(String line, Server server) {
        server.deleteCurrentUser();
    }

}
