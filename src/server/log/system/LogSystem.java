package server.log.system;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import user.Client;
import user.User;

public class LogSystem implements LogSystemFunctionality, Serializable {

    private Map<String, User> users = new HashMap<>();
    private User currentUser = null;

    public LogSystem() {
        this.currentUser = new User();
    }

    public boolean containsUser(String name) {
        return this.users.containsKey(name);
    }

    public User getCurrentUser() {
        return this.currentUser;
    }

    public boolean signUp(String name, String password) {
        if (name != null && password != null) {
            if (this.users.containsKey(name)) {
                System.out.println("A user with this nickname already exists!");
                return false;
            }
            this.users.put(name, new Client(name, password));
        }
        System.out.println("You signed up successfully!Sing in now and start shopping!");
        return true;
    }

    public boolean signIn(String name, String password) {
        if (name != null && password != null) {
            if (!this.users.containsKey(name)) {
                System.out.println("No such user!");
                return false;
            }

            if (this.users.get(name).correctPassword(password)) {
                this.currentUser = this.users.get(name);
                System.out.println("Welcome back, " + name + " !");
                System.out.println(
                        "You have " + this.currentUser.getNumberOfNewMsgs() + " new messages!");
            } else {
                System.out.println("Wrong password!");
                return false;
            }
        }
        return true;
    }

    public boolean deleteCurrentUser() {
        if (this.users.containsKey(this.currentUser.getName())) {
            this.users.remove(this.currentUser.getName());
            System.out.println("You have deleted your profile!");

            // Creating new object every time.Is it better than making the randomName function
            // package private?
            User user;
            do {
                user = new User();
            } while (this.users.containsKey(user.getName()));
            this.currentUser = user;
            return true;
        } else {
            System.out.println("You are not registered, you are a guest.");
            return false;
        }
    }

    @Override
    public void logout() {
        this.currentUser = new User();
        System.out.println("You have successfully logged out");
        System.out.println("Your temporary nickname is " + this.currentUser.getName() + " .");
    }

    public Map<String, User> getUsers() {
        return this.users;
    }

}
