package server.log.system;

interface LogSystemFunctionality {

    boolean signUp(String name, String password);

    boolean signIn(String name, String password);

    boolean deleteCurrentUser();

    void logout();
}
