package server;

public class CommandAddProduct implements CommandFunctionality {

    @Override
    public void execute(String line, Server server) {
        server.addProduct(line);
    }
}
