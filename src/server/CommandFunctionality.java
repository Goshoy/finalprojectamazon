package server;

public interface CommandFunctionality {

    void execute(String line, Server server);
}
