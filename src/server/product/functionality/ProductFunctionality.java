package server.product.functionality;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import product.Product;
import server.log.system.LogSystem;
import user.User;

public class ProductFunctionality implements Serializable {
    //
    // The command for adding product is -
    // addProduct <productName> <quantity> <description> <price> <pathToIcon>
    // <pathToIcon> is not obligatorisch.
    //

    private int currentId;
    private Map<Integer, Product> productsById;
    private Map<User, UserProducts> productsByUser;

    public ProductFunctionality() {
        this.productsById = new HashMap<>();
        this.productsByUser = new HashMap<>();
        this.currentId = 0;
    }

    boolean containsProduct(int id) {
        return this.productsById.containsKey(id);
    }

    public boolean addProduct(String line, LogSystem logSystem) {
        String[] args = line.split(" ");
        if (args.length < 4) {
            System.out.println("Inccorect input!");
            return false;
        }
        String description = parseDescription(line, args);
        String pathToIcon = parseIconPath(args[args.length - 1]);
        int quantity = parseInteger(args[1]);
        int price = parseInteger(args[2]);

        if (price == 0 || quantity == 0) {
            return false;
        }

        if (!isUserAdded(logSystem.getCurrentUser())) {
            this.productsByUser.put(logSystem.getCurrentUser(), new UserProducts());
            createAndAddProduct(args[0], description, price, logSystem.getCurrentUser(), quantity,
                    pathToIcon, logSystem);
            return true;
        }

        if (this.productsByUser.get(logSystem.getCurrentUser()).containsName(args[0])) {
            this.productsByUser.get(logSystem.getCurrentUser()).increaseQuantity(args[0], quantity);
            return true;
        }

        createAndAddProduct(args[0], description, price, logSystem.getCurrentUser(), quantity,
                pathToIcon, logSystem);
        return true;
    }

    int parseInteger(String line) {
        int result = 0;
        try {
            result = Integer.valueOf(line);
        } catch (NumberFormatException e) {
            System.out.println("Invalid quantity!");
            return 0;
        }
        return result;
    }

    String parseDescription(String line, String[] args) {
        String description = null;
        if (args[args.length - 1].contains("\\")) {
            description =
                    line.substring((args[0].length() + args[1].length() + args[2].length()) + 2,
                            line.length() - args[args.length - 1].length());
        } else

        {
            description = line.substring(
                    (args[0].length() + args[1].length() + args[2].length()) + 2, line.length());
        }
        return description;
    }

    String parseIconPath(String line) {
        if (line.contains("\\")) {
            return line;
        }
        return null;
    }

    boolean isUserAdded(User publisher) {
        return this.productsByUser.containsKey(publisher);
    }

    void createAndAddProduct(String name, String description, int price, User publisher,
            int quantity, String pathToIcon, LogSystem logSystem) {
        Random random = new Random();
        this.currentId = random.nextInt(10000);

        while (this.containsProduct(currentId)) {
            this.currentId = random.nextInt(10000);
        }

        Product product = new Product(this.currentId, name, description, price,
                logSystem.getCurrentUser(), pathToIcon);
        if (quantity > 1) {
            for (int i = 1; i < quantity; i++) {
                product.increaseQuantity();
            }
        }
        this.productsById.put(product.getId(), product);
        this.productsByUser.get(logSystem.getCurrentUser()).addProduct(product);

    }

    public boolean deleteProduct(int id, LogSystem logSystem) {
        if (!this.containsProduct(id)) {
            System.out.println("No such product!");
            return false;
        }

        if (!logSystem.getCurrentUser().equals(this.productsById.get(id).getPublisher())) {
            System.out.println("That is not your product, you cant delete it!");
            return false;
        }

        deleteProductHelper(id, logSystem);
        System.out.println("Product with id " + id + " was deleted!");
        return true;
    }

    private void deleteProductHelper(int id, LogSystem logSystem) {
        this.productsByUser.get(logSystem.getCurrentUser()).deleteProduct(id);
        this.productsById.remove(id);
    }

    public void show(int id) {
        if (!this.containsProduct(id)) {
            System.out.println("No such product!");
            return;
        }

        this.productsById.get(id).info();
    }

    public boolean buy(int id, LogSystem logSystem) {
        if (!this.containsProduct(id)) {
            System.out.println("No such product!");
            return false;
        }

        if (this.productsById.get(id).getPublisher().equals(logSystem.getCurrentUser())) {
            System.out.println("You can`t buy from yourself!");
            return false;
        }

        if (!isUserAdded(logSystem.getCurrentUser())) {
            this.productsByUser.put(logSystem.getCurrentUser(), new UserProducts());
        }

        this.productsByUser.get(logSystem.getCurrentUser()).buyProduct(this.productsById.get(id));

        if (this.productsById.get(id).decreaseQuantity()) {
            deleteProductHelper(id, logSystem);
        }

        return true;
    }

    public void showAllProducts() {
        this.productsById.entrySet().stream().forEach(p -> p.getValue().info());
    }

    public void showProductIcon(int id) {
        if (this.containsProduct(id)) {
            this.productsById.get(id).showIcon();
        } else {
            System.out.println("The product does not have icon!");
        }
    }

    public void showTopTenProducts() {
        this.productsById.entrySet().stream()
                .sorted(Map.Entry.<Integer, Product>comparingByValue().reversed()).limit(10)
                .forEach(p -> p.getValue().info());
    }

    public int getCurrentId() {
        return this.currentId;
    }

    public Map<User, UserProducts> getProductsByPublisher() {
        return this.productsByUser;
    }

    public void deleteUser(User user) {
        if (this.productsByUser.containsKey(user)) {
            this.productsByUser.get(user).getProductsById().entrySet().stream().forEach(p -> {
                int id = p.getKey();
                if (this.productsById.containsKey(id)) {
                    this.productsById.remove(id);
                }
            });

            this.productsByUser.remove(user);
        }
    }
}
