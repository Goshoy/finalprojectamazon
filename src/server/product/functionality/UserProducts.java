package server.product.functionality;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;
import product.Product;

public class UserProducts implements Serializable {
    // bought products
    private Map<Integer, Product> boughtProducts;
    // products for sale;
    private Map<Integer, String> productsById;
    private Map<String, Product> productsByName;

    public UserProducts() {
        this.boughtProducts = new HashMap<>();
        this.productsById = new HashMap<>();
        this.productsByName = new HashMap<>();
    }

    public void addProduct(Product product) {
        this.productsById.put(product.getId(), product.getName());
        this.productsByName.put(product.getName(), product);
    }

    public boolean containsName(String name) {
        return this.productsByName.containsKey(name);
    }

    public void increaseQuantity(String name, int times) {
        for (int i = 0; i < times; i++) {
            this.productsByName.get(name).increaseQuantity();
        }
    }

    public int getQuantityOfBoughtProduct(int id) {
        return this.boughtProducts.get(id).getQuantity();
    }


    public int getQuantityOfProductForSale(int id) {
        return this.productsByName.get(this.productsById.get(id)).getQuantity();
    }


    public void deleteProduct(int id) {
        this.productsByName.remove(this.productsById.get(id));
        this.productsById.remove(id);
    }

    public void buyProduct(Product product) {
        if (this.boughtProducts.containsKey(product.getId())) {
            this.boughtProducts.get(product.getId()).increaseQuantity();
            return;
        }

        this.boughtProducts.put(product.getId(), product.clone());
    }

    public boolean sellProduct(Product product) {
        return this.productsByName.get(product.getId()).decreaseQuantity();
    }

    public double getAveragePriceOfProductForSale() {

        // hardcode over level...
        Map<String, Product> copy = new HashMap<>();
        copy.putAll(this.productsByName);
        copy.entrySet().stream().forEach(s -> {
            if (s.getValue().getQuantity() == 0) {
                this.productsByName.remove(s.getValue().getName());
            }
        });

        Double result = this.productsByName.entrySet().stream()
                .collect(Collectors.averagingInt(p -> p.getValue().getPrice()));
        return result;
    }

    public double getAveragePriceOfBoughtProducts() {
        Double result = this.boughtProducts.entrySet().stream()
                .collect(Collectors.averagingInt(p -> p.getValue().getPrice()));
        return result;
    }

    Map<Integer, String> getProductsById() {
        return this.productsById;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((boughtProducts == null) ? 0 : boughtProducts.hashCode());
        result = prime * result + ((productsById == null) ? 0 : productsById.hashCode());
        result = prime * result + ((productsByName == null) ? 0 : productsByName.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        UserProducts other = (UserProducts) obj;
        if (boughtProducts == null) {
            if (other.boughtProducts != null)
                return false;
        } else if (!boughtProducts.equals(other.boughtProducts))
            return false;
        if (productsById == null) {
            if (other.productsById != null)
                return false;
        } else if (!productsById.equals(other.productsById))
            return false;
        if (productsByName == null) {
            if (other.productsByName != null)
                return false;
        } else if (!productsByName.equals(other.productsByName))
            return false;
        return true;
    }

}
