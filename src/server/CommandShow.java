package server;

public class CommandShow implements CommandFunctionality {

    @Override
    public void execute(String line, Server server) {
        server.show(line);
    }

}
