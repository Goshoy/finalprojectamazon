package server;

public class CommandSignUp implements CommandFunctionality {

    @Override
    public void execute(String line, Server server) {
        String[] args = line.split(" ");
        server.SignUp(args[0], args[1]);
    }

}
