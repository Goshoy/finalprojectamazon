package server;

public class CommandShowAllProducts implements CommandFunctionality {

    @Override
    public void execute(String line, Server server) {
        server.showAllProducts();
    }

}
