package server;

public class CommandSignIn implements CommandFunctionality {

    @Override
    public void execute(String line, Server server) {
        String[] args = line.split(" ");
        server.SignIn(args[0], args[1]);
    }

}
