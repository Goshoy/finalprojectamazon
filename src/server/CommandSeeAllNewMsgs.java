package server;

public class CommandSeeAllNewMsgs implements CommandFunctionality {

    @Override
    public void execute(String line, Server server) {
        server.seeAllMyMessages();
    }

}
