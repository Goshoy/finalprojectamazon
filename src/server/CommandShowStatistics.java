package server;

public class CommandShowStatistics implements CommandFunctionality {

    @Override
    public void execute(String line, Server server) {
        server.showStatistics();
    }

}
