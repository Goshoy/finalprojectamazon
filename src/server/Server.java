package server;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import server.log.system.LogSystem;
import server.member.functionality.MemberFunctionality;
import server.product.functionality.ProductFunctionality;
import user.User;

public class Server {

    private final String usersFilePath =
            "C:\\Users\\Goshoy\\eclipse-workspace\\FinalProjectAmazon\\users.txt";
    private final String productsFilePath =
            "C:\\Users\\Goshoy\\eclipse-workspace\\FinalProjectAmazon\\products.txt";
    private final String name;
    private LogSystem logSystem;
    private final MemberFunctionality memberFunctionality;
    private ProductFunctionality productFunctionality;
    private final Executor executor;

    private boolean saveUsers = false;
    private boolean saveProducts = false;

    public Server() {
        this.name = "Amazon";
        initLogSystem();
        initProducts();
        this.memberFunctionality = new MemberFunctionality(this.logSystem);
        this.executor = new Executor();
    }

    void initLogSystem() {
        try (ObjectInputStream reader = new ObjectInputStream(new FileInputStream(usersFilePath))) {

            this.logSystem = (LogSystem) reader.readObject();
            return;
        } catch (Exception e) {
            System.out.println("Failed loading users!");
        }
        this.logSystem = new LogSystem();
    }

    void initProducts() {
        try (ObjectInputStream reader =
                new ObjectInputStream(new FileInputStream(productsFilePath))) {

            this.productFunctionality = (ProductFunctionality) reader.readObject();
            return;
        } catch (Exception e) {
            System.out.println("Failed loading products!");
        }

        this.productFunctionality = new ProductFunctionality();
    }

    boolean containsUser(String name) {
        return this.logSystem.containsUser(name);
    }

    User getCurrentUser() {
        return this.logSystem.getCurrentUser();
    }

    boolean SignUp(String name, String password) {
        boolean result = this.logSystem.signUp(name, password);
        if (this.saveUsers == false) {
            this.saveUsers = result;
        }
        return result;
    }

    boolean SignIn(String name, String password) {
        boolean result = this.logSystem.signIn(name, password);
        if (result) {
            this.productFunctionality.showTopTenProducts();
        }
        return result;
    }

    void deleteCurrentUser() {
        User user = this.logSystem.getCurrentUser();
        if (this.logSystem.deleteCurrentUser()) {
            this.productFunctionality.deleteUser(user);
        }
    }

    void writeMessage(String nameToWriteTo, String message) {
        memberFunctionality.writeMessage(nameToWriteTo, message);
    }

    void logout() {
        this.logSystem.logout();
    }

    void show(String name) {
        this.memberFunctionality.show(name);
    }

    void seeAllMyMessages() {
        this.memberFunctionality.seeAllNewMessages();
    }

    void addProduct(String line) {
        if (saveProducts == false) {
            saveProducts = this.productFunctionality.addProduct(line, this.logSystem);
        } else {
            this.productFunctionality.addProduct(line, this.logSystem);
        }
    }

    void showAllProducts() {
        this.productFunctionality.showAllProducts();
    }

    void buy(int id) {
        if (saveProducts == false) {
            saveProducts = this.productFunctionality.buy(id, this.logSystem);
        } else {
            this.productFunctionality.buy(id, this.logSystem);
        }
    }

    void deleteProduct(int id) {
        if (saveProducts == false) {
            saveProducts = this.productFunctionality.deleteProduct(id, this.logSystem);
        } else {
            this.productFunctionality.deleteProduct(id, this.logSystem);
        }
    }

    void showProduct(int id) {
        this.productFunctionality.show(id);
    }

    void showProductIcon(int id) {
        this.productFunctionality.showProductIcon(id);
    }

    public void showStatistics() {
        this.memberFunctionality.showStatistics(this.productFunctionality.getProductsByPublisher());
    }

    public void executeCommand(String line) {
        this.executor.executeCommand(line, this);
    }


    public void save() {
        this.saveUsers();
        this.saveProducts();
    }


    public void saveUsers() {
        if (this.saveUsers) {
            try (ObjectOutputStream writer =
                    new ObjectOutputStream(new FileOutputStream(usersFilePath))) {
                writer.writeObject(this.logSystem);
            } catch (IOException e) {
                System.out.println("Something went wrong when saving products!");
            }
        }
    }

    private void saveProducts() {
        if (this.saveProducts) {
            try (ObjectOutputStream writer =
                    new ObjectOutputStream(new FileOutputStream(productsFilePath))) {
                writer.writeObject(this.productFunctionality);
            } catch (IOException e) {
                System.out.println("Something went wrong when saving products!");
            }
        }
    }


    public String getName() {
        return this.name;
    }
}
