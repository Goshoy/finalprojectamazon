package server;

import server.command.parser.CommandParserMain;

public class Executor {
    CommandParserMain commandParser = new CommandParserMain();
    CommandFunctionality command = null;

    void executeCommand(String line, Server server) {
        String[] args = line.split(" ");
        command = commandParser.generateInstance(args[0]);

        if (command != null) {
            if (args.length > 1) {
                line = line.substring(args[0].length() + 1);
            } else {
                line = line.substring(args[0].length());
            }
            command.execute(line, server);
        }
    }
}
