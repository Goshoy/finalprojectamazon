package server;

public class CommandLogOut implements CommandFunctionality {

    @Override
    public void execute(String line, Server server) {
        server.logout();
    }

}
