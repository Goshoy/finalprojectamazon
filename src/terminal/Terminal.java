package terminal;

import java.util.Scanner;
import server.Server;

public class Terminal {
    private final Server server;

    public Terminal() {
        this.server = new Server();
    }

    public void execute(String line) {
        this.server.executeCommand(line);
    }

    public void start() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Welcome!");
        System.out.println("To sign up type signUp name password,");
        System.out.println("to sign in type signIp name password,");
        System.out.println("or just continue browsing as a guest.");

        String line = scanner.nextLine();
        while (!line.equals("quit")) {
            this.execute(line);
            line = scanner.nextLine();
        }
        server.save();
    }
}
