package product;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import user.User;

public class Product implements Serializable, Comparable<Product> {

    private final int id;
    private final String name;
    private final String description;
    private final int price;
    private final User publisher;
    private final String pathToIcon;
    private int quantity;
    private int timesSold;

    public Product(int id, String name, String description, int price, User publisher,
            String path) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.price = price;
        this.publisher = publisher;
        this.pathToIcon = path;
        this.quantity = 1;
        this.timesSold = 0;
    }

    public int getId() {
        return this.id;
    }

    public String getName() {
        return this.name;
    }

    public String getDescription() {
        return this.description;
    }

    public int getPrice() {
        return this.price;
    }

    public User getPublisher() {
        return this.publisher;
    }

    public int getQuantity() {
        return this.quantity;
    }

    public int getTimesSold() {
        return this.timesSold;
    }

    public void increaseQuantity() {
        this.quantity++;
    }

    public boolean decreaseQuantity() {
        this.quantity--;
        this.timesSold++;
        return this.quantity == 0;
    }

    public void showIcon() {
        if (this.pathToIcon != null) {
            File file = new File(pathToIcon);
            BufferedImage image = null;

            try {
                image = ImageIO.read(file);
            } catch (IOException e) {
                System.out.println("Failed loading image!");
                return;
            }

            JLabel label = new JLabel(new ImageIcon(image));
            JFrame f = new JFrame();
            f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            f.getContentPane().add(label);
            f.pack();
            f.setLocation(500, 500);
            f.setVisible(true);
        }
    }

    public Product clone() {
        return new Product(this.id, this.name, this.description, this.price, this.publisher,
                this.pathToIcon);
    }

    public void info() {
        System.out.println("ProductId: " + this.id);
        System.out.println("  name: " + this.name);
        System.out.println("  description: " + this.description);
        System.out.println("  price: " + this.price);
        System.out.println("  quantity: " + this.quantity + ".");
        System.out.println();
    }

    @Override
    public int compareTo(Product o) {
        return Integer.valueOf(this.timesSold).compareTo(o.timesSold);
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((description == null) ? 0 : description.hashCode());
        result = prime * result + id;
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        result = prime * result + ((pathToIcon == null) ? 0 : pathToIcon.hashCode());
        result = prime * result + price;
        result = prime * result + ((publisher == null) ? 0 : publisher.hashCode());
        result = prime * result + quantity;
        result = prime * result + timesSold;
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Product other = (Product) obj;
        if (description == null) {
            if (other.description != null)
                return false;
        } else if (!description.equals(other.description))
            return false;
        if (id != other.id)
            return false;
        if (name == null) {
            if (other.name != null)
                return false;
        } else if (!name.equals(other.name))
            return false;
        if (pathToIcon == null) {
            if (other.pathToIcon != null)
                return false;
        } else if (!pathToIcon.equals(other.pathToIcon))
            return false;
        if (price != other.price)
            return false;
        if (publisher == null) {
            if (other.publisher != null)
                return false;
        } else if (!publisher.equals(other.publisher))
            return false;
        if (quantity != other.quantity)
            return false;
        if (timesSold != other.timesSold)
            return false;
        return true;
    }



}
